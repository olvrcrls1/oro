## About This oRO repository

**This is where the official progress tracking and reporting of bugs for oRagnarok Online server.**


## How to report an in-game bug
- Visit this link (https://gitlab.com/olvrcrls1/oro/-/issues)
- Click the button *New Issue*
- Please provide the brief title of the bug.
- Leave the *Type* to be an "Issue"
- On the description, describe the bug briefly, tell if you're using a customized GRF or not, and steps on *How to Reproduce* the bug.
- Providing evidence material would be a great help for the progress. Thank you.


### Administrators to Contact at Discord (https://discord.gg/n6cJ3jBsqb)
* [Andie#4170]
* [Wazaby#8934]
